﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Forum.BLL.DTO
{
    public class CommentDTO
    {
        public int Id { get; set; }

        public string UserId { get; set; }
        public string UserFullName {  get; set; }
        public UserDTO User { get; set; }

        public int DiscussionId { get; set; }
        public DiscussionDTO Discussion { get; set; }

        public string ComentMessage { get; set; }
        public DateTime DateComent { get; set; }

        public int CountLikesComment { get; set; }
        public int CountDislikesComment { get; set; }
    }
}
