﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Forum.BLL.DTO
{
    public class DiscussionCategoryDTO
    {
        public int Id { get; set; }

        public string CategotyName { get; set; }

        public List<DiscussionTopicDTO> Topics { get; set; } = new List<DiscussionTopicDTO>();
    }
}
