﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Forum.BLL.DTO
{
    public class DiscussionDTO
    {
        public int Id { get; set; }

        public string UserId { get; set; }

        public string UserFullName { get; set; }

        public string DiscussionMessage { get; set; }

        public DateTime DiscussionDate { get; set; }

        public bool IsDiscussed { get; set; }

        public int TopicId { get; set; }
        public DiscussionTopicDTO Topic { get; set; }

        public List<CommentDTO> Comments { get; set; }
    }
}
