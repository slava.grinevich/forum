﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Forum.BLL.DTO
{
    public class DiscussionTopicDTO
    {
        public int Id { get; set; }
        public string Topic { get; set; }

        public int IdCategory { get; set; }
        public DiscussionCategoryDTO DiscussionCategory { get; set; }

        public List<DiscussionDTO> Discussions { get; set; } = new List<DiscussionDTO>();
    }
}
