﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Forum.BLL.Exceptions
{
    public class ValidationException : Exception
    {
        public ValidationException(string message, string prop) : base($"{prop} : {message}")
        {
        }
    }
}
