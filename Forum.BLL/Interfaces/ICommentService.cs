﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Forum.BLL.DTO;

namespace Forum.BLL.Interfaces
{
    public interface ICommentService
    {
        
        Task<IEnumerable<CommentDTO>> GetAllCommentsByUsersIdAsync(string UserId);

        Task<IEnumerable<CommentDTO>> GetAllCommentsByDiscussionIdAsync(int DiscussionId);

        Task AddLike(int CommentId);
        Task AddDislike(int CommentId);

        Task<IEnumerable<CommentDTO>> GetAllAsync();
        Task<CommentDTO> GetAsync(int id);
        Task CreateAsync(CommentDTO item);
        Task UpdateAsync(CommentDTO item);
        Task Delete(int commentId);

    }
}
