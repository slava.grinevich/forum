﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Forum.BLL.DTO;

namespace Forum.BLL.Interfaces
{
    public interface IDiscussionCategoryService 
    {
        Task<IEnumerable<DiscussionTopicDTO>> GetAllDiscussionTopicOfDiscussionCategoryAsync(int DiscussionCategoryId);
        Task<DiscussionCategoryDTO> GetDiscussionCategoryOfDiscussionTopicAsync(int TopicId);
        Task<int> CountAllDiscussionOfCategoryId(int DiscussionCategoryId);


        Task<IEnumerable<DiscussionCategoryDTO>> GetAllAsync();
        Task<DiscussionCategoryDTO> GetAsync(int id);
        Task CreateAsync(DiscussionCategoryDTO item);
        Task Update(DiscussionCategoryDTO item);
        Task Delete(int categotyId);
    }
}
