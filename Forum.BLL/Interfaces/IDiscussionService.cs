﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Forum.BLL.DTO;

namespace Forum.BLL.Interfaces
{
    public interface IDiscussionService
    {
        Task<IEnumerable<DiscussionDTO>> GetAllDiscussionByUserIdAsync(string UserId);
        Task<IEnumerable<DiscussionDTO>> GetAllDiscussionOfDiscussionTopicAsync(int TopicId);

        Task<IEnumerable<DiscussionDTO>> GetAllAsync();
        Task<DiscussionDTO> GetAsync(int id);
        Task CreateAsync(DiscussionDTO item);
        Task Update(DiscussionDTO item);
        Task Delete(int discussionId);
    }
}
