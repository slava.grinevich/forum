﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Forum.BLL.DTO;

namespace Forum.BLL.Interfaces
{
    public interface IDiscussionTopicService
    {
        Task<DiscussionTopicDTO> GetDiscussionTopicByDiscussionIdAsync(int DiscussionId);

        Task<IEnumerable<DiscussionTopicDTO>> GetAllAsync();
        Task<DiscussionTopicDTO> GetAsync(int id);
        Task CreateAsync(DiscussionTopicDTO item);
        Task Update(DiscussionTopicDTO item);
        Task Delete(int topicId);
    }
}
