﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Forum.BLL.DTO;

namespace Forum.BLL.Interfaces
{
    public interface IUserService
    {
        Task CountAllRatingCommentsByUserIdAsync(string UserId);

        Task<IEnumerable<UserDTO>> GetAllAsync();
        Task<UserDTO> GetAsync(int id);
        Task CreateAsync(UserDTO item);
        Task Update(UserDTO item);
        Task Delete(int userId);
    }
}
