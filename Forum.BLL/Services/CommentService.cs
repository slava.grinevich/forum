﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using Forum.BLL.DTO;
using Forum.BLL.Interfaces;
using Forum.BLL.Exceptions ;
using Forum.DAL.Entities;
using Forum.DAL.Interfaces;
using Forum.DAL.Repositories;

namespace Forum.BLL.Services
{
    public class CommentService : ICommentService
    {
        private readonly IUnitOfWork _database;
        private readonly IMapper _mapper;

        public CommentService(
            IUnitOfWork ForumUnitOfWork,
            IMapper mapper)
        {
            _database = ForumUnitOfWork;
            _mapper = mapper;
        }

        public async Task<IEnumerable<CommentDTO>> GetAllCommentsByDiscussionIdAsync(int DiscussionId)
        {
            var AllComentByDiscussId = await _database.Comments.GetAllCommentsByDiscussionIdAsync(DiscussionId);
            return _mapper.Map<IEnumerable<CommentDTO>>(AllComentByDiscussId);
        }

        public async Task<IEnumerable<CommentDTO>> GetAllCommentsByUsersIdAsync(string UserId)
        {
            var AllComentByUserId = await _database.Comments.GetAllCommentsByUsersIdAsync(UserId);
            return _mapper.Map<IEnumerable<CommentDTO>>(AllComentByUserId);
        }
                

        public async Task<IEnumerable<CommentDTO>> GetAllAsync()
        {
            var AllCom = await _database.Comments.GetAllAsync();
            return _mapper.Map<IEnumerable<CommentDTO>>(AllCom);
        }

        public async Task<CommentDTO> GetAsync(int id)
        {
            var Com = await _database.Comments.GetAsync(id);
            return _mapper.Map<CommentDTO>(Com);
        }


        public async Task CreateAsync(CommentDTO item)
        {
            if(item == null)
                throw new Exceptions.ValidationException("Comment can't be null", nameof(item));

            if (item.ComentMessage == null)
                throw new Exceptions.ValidationException("Comment Message can't be null", nameof(item.ComentMessage));

            await _database.Comments.CreateAsync(_mapper.Map<Comment>(item));
            await _database.SaveAsync();
        }

        public async Task Delete(int commentId)
        {
            if (commentId == 0)
                throw new Exceptions.ValidationException("Comment id can't be 0", nameof(commentId));

            Comment dellComment = _database.Comments.GetAsync(commentId).Result;

            if (dellComment == null)
                throw new Exceptions.ValidationException("Comment can't be null", nameof(dellComment));

            _database.Comments.Detach(dellComment);
            _database.Comments.Delete(dellComment);
            await _database.SaveAsync();
        }

        public async Task UpdateAsync(CommentDTO item)
        {
            if (item == null)
                throw new Exceptions.ValidationException("Comment can't be null", nameof(item));

            if (item.ComentMessage == null)
                throw new Exceptions.ValidationException("Comment Message can't be null", nameof(item.ComentMessage));

            _database.Comments.Update(_mapper.Map<Comment>(item));
            await _database.SaveAsync();
        }

        public async Task AddLike(int CommentId)
        {
            var tmp = await _database.Comments.GetAsync(CommentId);

            if (tmp == null)
                throw new Exceptions.ValidationException("Comment can't be null", nameof(tmp));
            
            tmp.CountLikesComment += 1;

            _database.Comments.Update(_mapper.Map<Comment>(tmp));
            await _database.SaveAsync();
        }


        public async Task AddDislike(int CommentId)
        {
            var tmp = await _database.Comments.GetAsync(CommentId);

            if (tmp == null)
                throw new Exceptions.ValidationException("Comment can't be null", nameof(tmp));

            tmp.CountDislikesComment += 1;

            _database.Comments.Update(_mapper.Map<Comment>(tmp));
            await _database.SaveAsync();
        }
    }
}
