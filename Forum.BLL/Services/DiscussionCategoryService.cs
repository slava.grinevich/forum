﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using Forum.BLL.DTO;
using Forum.BLL.Interfaces;
using Forum.DAL.Entities;
using Forum.DAL.Interfaces;

namespace Forum.BLL.Services
{
    public class DiscussionCategoryService : IDiscussionCategoryService
    {
        private readonly IUnitOfWork _database;
        private readonly IMapper _mapper;

        public DiscussionCategoryService(
            IUnitOfWork ForumUnitOfWork,
            IMapper mapper)
        {
            _database = ForumUnitOfWork;
            _mapper = mapper;
        }

        public async Task<IEnumerable<DiscussionTopicDTO>> GetAllDiscussionTopicOfDiscussionCategoryAsync(int DiscussionCategoryId)
        {
            if (DiscussionCategoryId == 0)
                throw new Exceptions.ValidationException("Id category can't be 0", nameof(DiscussionCategoryId));

            var AllDisccussionTopicsOfCateory = await _database.DiscussionCategory.GetAllDiscussionTopicOfDiscussionCategoryAsync(DiscussionCategoryId);
            return _mapper.Map<IEnumerable<DiscussionTopicDTO>>(AllDisccussionTopicsOfCateory);
        }

        public async Task<DiscussionCategoryDTO> GetDiscussionCategoryOfDiscussionTopicAsync(int TopicId)
        {
            if (TopicId == 0)
                throw new Exceptions.ValidationException("Id topic can't be 0", nameof(TopicId));

            var DisccussionCategOfTopic = await _database.DiscussionCategory.GetDiscussionCategoryOfDiscussionTopicAsync(TopicId);
            return _mapper.Map<DiscussionCategoryDTO>(DisccussionCategOfTopic);
        }

        public async Task<int> CountAllDiscussionOfCategoryId(int DiscussionCategoryId)
        {
            if (DiscussionCategoryId == 0)
                throw new Exceptions.ValidationException("Id category can't be 0", nameof(DiscussionCategoryId));

            int sum = 0;
            var tmp = await GetAllDiscussionTopicOfDiscussionCategoryAsync(DiscussionCategoryId);

            foreach (var i in tmp)
            {
                i.Discussions = _mapper.Map<List<DiscussionDTO>>(_database.Discussion.GetAllDiscussionOfDiscussionTopicAsync(i.Id).Result);
                sum += i.Discussions.Count();
            }

            return sum;
        }



        public async Task<IEnumerable<DiscussionCategoryDTO>> GetAllAsync()
        {
            var AllCom = await _database.DiscussionCategory.GetAllAsync();
            return _mapper.Map<IEnumerable<DiscussionCategoryDTO>>(AllCom);
        }

        public async Task<DiscussionCategoryDTO> GetAsync(int id)
        {
            if (id == 0)
                throw new Exceptions.ValidationException("Category id can't be 0", nameof(id));

            var Com = await _database.DiscussionCategory.GetAsync(id);
            return _mapper.Map<DiscussionCategoryDTO>(Com);
        }


        public async Task CreateAsync(DiscussionCategoryDTO item)
        {
            if (item == null)
                throw new Exceptions.ValidationException("Category can't be null", nameof(item));

            if (item.CategotyName == null)
                throw new Exceptions.ValidationException("Categoty name can't be null", nameof(item.CategotyName));

            DiscussionCategory tmp = _mapper.Map<DiscussionCategory>(item);
            
            await _database.DiscussionCategory.CreateAsync(tmp);
            await _database.SaveAsync();
        }

        public async Task Delete(int categotyId)
        {
            if (categotyId == 0)
                throw new Exceptions.ValidationException("Categoty id can't be null", nameof(categotyId));

            DiscussionCategory dellCategoty = await _database.DiscussionCategory.GetAsync(categotyId);

            if (dellCategoty == null)
                throw new Exceptions.ValidationException("Categoty can't be null", nameof(dellCategoty));

            _database.DiscussionCategory.Detach(dellCategoty);
            _database.DiscussionCategory.Delete(dellCategoty);
            await _database.SaveAsync();
        }

        public async Task Update(DiscussionCategoryDTO item)
        {
            var allCategory = await _database.DiscussionCategory.GetAllAsync();
            
            if(allCategory.Where(x=>x.CategotyName==item.CategotyName).Count()>0)
                throw new Exceptions.ValidationException("Category is not unique", nameof(item.CategotyName));

            _database.DiscussionCategory.Update(_mapper.Map<DiscussionCategory>(item));
            await _database.SaveAsync();
        }
    }
}
