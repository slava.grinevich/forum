﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using Forum.BLL.DTO;
using Forum.BLL.Interfaces;
using Forum.DAL.Entities;
using Forum.DAL.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace Forum.BLL.Services
{
    public class DiscussionService: IDiscussionService
    {
        private readonly IUnitOfWork _database;
        private readonly IMapper _mapper;

        public DiscussionService(
            IUnitOfWork ForumUnitOfWork,
            IMapper mapper)
        {
            _database = ForumUnitOfWork;
            _mapper = mapper;
        }

        public async Task<IEnumerable<DiscussionDTO>> GetAllDiscussionByUserIdAsync(string UserId)
        {
            if (UserId == null)
                throw new Exceptions.ValidationException("User id can't be null", nameof(UserId));

            var AllDiscussionByUser = await _database.Discussion.GetAllDiscussionByUserIdAsync(UserId);
            return _mapper.Map<IEnumerable<DiscussionDTO>>(AllDiscussionByUser);
        }

        public async Task<IEnumerable<DiscussionDTO>> GetAllDiscussionOfDiscussionTopicAsync(int TopicId)
        {
            if (TopicId == 0)
                throw new Exceptions.ValidationException("Topic id can't be 0", nameof(TopicId));

            var AllDiscussionByTopic = await _database.Discussion.GetAllDiscussionOfDiscussionTopicAsync(TopicId);
            return _mapper.Map<IEnumerable<DiscussionDTO>>(AllDiscussionByTopic);
        }



        public async Task<IEnumerable<DiscussionDTO>> GetAllAsync()
        {
            var AllCom = await _database.Discussion.GetAllAsync();
            return _mapper.Map<IEnumerable<DiscussionDTO>>(AllCom);
        }

        public async Task<DiscussionDTO> GetAsync(int id)
        {
            if (id == 0)
                throw new Exceptions.ValidationException("Discussion id can't be 0", nameof(id));

            var Com = await _database.Discussion.GetAsync(id);

            if (Com == null)
                throw new Exceptions.ValidationException("Discussion can't be null", nameof(Com));

            return _mapper.Map<DiscussionDTO>(Com);
        }


        public async Task CreateAsync(DiscussionDTO item)
        {
            if (item == null)
                throw new Exceptions.ValidationException("Discussion can't be null", nameof(item));

            await _database.Discussion.CreateAsync(_mapper.Map<Discussion>(item));
            await _database.SaveAsync();
        }
         
        public async Task Delete(int discussionId)
        {
            if (discussionId == 0)
                throw new Exceptions.ValidationException("Discussion id can't be 0", nameof(discussionId));

            Discussion dellDisc = await _database.Discussion.GetAsync(discussionId);

            if (dellDisc == null)
                throw new Exceptions.ValidationException("Discussion can't be null", nameof(dellDisc));

            _database.Discussion.Detach(dellDisc);
            _database.Discussion.Delete(dellDisc);
            await _database.SaveAsync();
        }

        public async Task Update(DiscussionDTO item)
        {
            _database.Discussion.Update(_mapper.Map<Discussion>(item));
            await _database.SaveAsync();
        }
    }
}
