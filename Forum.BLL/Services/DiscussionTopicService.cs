﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using Forum.BLL.DTO;
using Forum.BLL.Interfaces;
using Forum.DAL.Entities;
using Forum.DAL.Interfaces;

namespace Forum.BLL.Services
{
    public class DiscussionTopicService : IDiscussionTopicService
    {
        private readonly IUnitOfWork _database;
        private readonly IMapper _mapper;

        public DiscussionTopicService(
            IUnitOfWork ForumUnitOfWork,
            IMapper mapper)
        {
            _database = ForumUnitOfWork;
            _mapper = mapper;
        }

        public async Task<DiscussionTopicDTO> GetDiscussionTopicByDiscussionIdAsync(int DiscussionId)
        {
            if (DiscussionId == 0)
                throw new Exceptions.ValidationException("Topic id can't be 0", nameof(DiscussionId));

            var DiscussionTopicByDiscussion = await _database.DiscussionTopic.GetDiscussionTopicByDiscussionIdAsync(DiscussionId);

            if (DiscussionTopicByDiscussion == null)
                throw new Exceptions.ValidationException("Discussion topic can't be null", nameof(DiscussionTopicByDiscussion));

            return _mapper.Map<DiscussionTopicDTO>(DiscussionTopicByDiscussion);
        }



        public async Task<IEnumerable<DiscussionTopicDTO>> GetAllAsync()
        {
            var AllCom = await _database.DiscussionTopic.GetAllAsync();
            return _mapper.Map<IEnumerable<DiscussionTopicDTO>>(AllCom);
        }

        public async Task<DiscussionTopicDTO> GetAsync(int id)
        {
            if (id == 0)
                throw new Exceptions.ValidationException("Topic id can't be 0", nameof(id));

            var Com = await _database.DiscussionTopic.GetAsync(id);

            if (Com == null)
                throw new Exceptions.ValidationException("Topic can't be null", nameof(Com));

            return _mapper.Map<DiscussionTopicDTO>(Com);
        }


        public async Task CreateAsync(DiscussionTopicDTO item)
        {
            if (item == null)
                throw new Exceptions.ValidationException("Topic can't be null", nameof(item));

            await _database.DiscussionTopic.CreateAsync(_mapper.Map<DiscussionTopic>(item));
            await _database.SaveAsync();
        }

        public async Task Delete(int topicId)
        {
            if (topicId == 0)
                throw new Exceptions.ValidationException("Topic id can't be 0", nameof(topicId));

            DiscussionTopic dellTopic = await _database.DiscussionTopic.GetAsync(topicId);

            if (dellTopic == null)
                throw new Exceptions.ValidationException("Topic can't be null", nameof(dellTopic));

            _database.DiscussionTopic.Detach(dellTopic);
            _database.DiscussionTopic.Delete(dellTopic);
            await _database.SaveAsync();
        }

        public async Task Update(DiscussionTopicDTO item)
        {

            var allCategory = await _database.DiscussionTopic.GetAllAsync();

            if (allCategory.Where(x => x.Topic == item.Topic).Count() > 0)
                throw new Exceptions.ValidationException("Topic is not unique", nameof(item.Topic));

            _database.DiscussionTopic.Update(_mapper.Map<DiscussionTopic>(item));
            await _database.SaveAsync();
        }

        
    }
}
