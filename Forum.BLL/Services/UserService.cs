﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Forum.BLL.Interfaces;
using Forum.BLL.DTO;
using Forum.DAL.Entities;
using AutoMapper;
using Forum.DAL.Interfaces;
using Forum.BLL.Services;
using Microsoft.AspNetCore.Identity;

namespace Forum.BLL.Services
{
    public class UserService : IUserService
    {
        private readonly IUnitOfWork _database;
        private readonly IMapper _mapper;

        public UserService(
            IUnitOfWork ForumUnitOfWork,
            IMapper mapper)
        {
            _database = ForumUnitOfWork;
            _mapper = mapper;
        }

        public async Task CountAllRatingCommentsByUserIdAsync(string UserId)
        {
            if (UserId == null)
                throw new Exceptions.ValidationException("User id can't be null", nameof(UserId));

            CommentService coments = new CommentService(_database, _mapper);
            var AllComentsUser = await coments.GetAllCommentsByUsersIdAsync(UserId);
            double countRating = 0;
            foreach (var i in AllComentsUser)
            {
                double onepercent = i.CountDislikesComment + i.CountLikesComment;
                onepercent /= 100;
                int generalValue = i.CountLikesComment - i.CountDislikesComment;
                bool isNegative = false;
                if (generalValue <= 0)
                {
                    isNegative = true;
                    generalValue = Math.Abs(generalValue);
                }
                double countOfPercent = generalValue / onepercent;
                if (isNegative)
                    countRating += - 1 * (0.1 * countOfPercent);
                else
                    countRating += 0.1 * countOfPercent;

            }

            var tmp = await _database.User.GetAsync(UserId);
            tmp.Rating = Math.Round(countRating, 3);
            
            _database.User.Update(tmp);
            await _database.SaveAsync();
        }


        public async Task CreateAsync(UserDTO item)
        {
            if (item == null)
                throw new Exceptions.ValidationException("User can't be null", nameof(item));

            var AllUsers = await _database.User.GetAllAsync();

            if(AllUsers.Where(x=>x.Email == item.Email).Count()>0)
                throw new Exceptions.ValidationException("User email is not unique", nameof(item.Email));

            if (AllUsers.Where(x => x.UserName == item.UserName).Count() > 0)
                throw new Exceptions.ValidationException("UserName is not unique", nameof(item.UserName));

            await _database.User.CreateAsync(_mapper.Map<User>(item));
            await _database.SaveAsync();
        }

        public async Task Delete(int userId)
        {
            if (userId == 0)
                throw new Exceptions.ValidationException("User id can't be 0", nameof(userId));

            User dellUser = await _database.User.GetAsync(userId);

            if (dellUser == null)
                throw new Exceptions.ValidationException("User can't be null", nameof(dellUser));

            _database.User.Detach(dellUser);
            _database.User.Delete(dellUser);
            await _database.SaveAsync();
        }

        public async Task<IEnumerable<UserDTO>> GetAllAsync()
        {
            var AllUsers = await _database.User.GetAllAsync();
            return _mapper.Map<IEnumerable<UserDTO>>(AllUsers);
        }

        public async Task<UserDTO> GetAsync(int id)
        {
            if (id == 0)
                throw new Exceptions.ValidationException("User id can't be 0", nameof(id));

            var User = await _database.User.GetAsync(id);

            if (User == null)
                throw new Exceptions.ValidationException("User can't be null", nameof(User));

            return _mapper.Map<UserDTO>(User);
        }

        public async Task Update(UserDTO item)
        {

            if (item == null)
                throw new Exceptions.ValidationException("User can't be null", nameof(item));

            var AllUsers = await _database.User.GetAllAsync();

            if (AllUsers.Where(x => x.Email == item.Email).Count() > 0)
                throw new Exceptions.ValidationException("User email is not unique", nameof(item.Email));

            if (AllUsers.Where(x => x.UserName == item.UserName).Count() > 0)
                throw new Exceptions.ValidationException("UserName is not unique", nameof(item.UserName));

            _database.User.Update(_mapper.Map<User>(item));
            await _database.SaveAsync();
        }
    }
}
