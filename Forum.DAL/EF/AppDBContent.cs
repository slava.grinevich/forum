﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Forum.DAL.Entities;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;

namespace Forum.DAL.EF
{
    public class AppDBContent : IdentityDbContext<User>
    {

        public AppDBContent(DbContextOptions<AppDBContent> options): base(options) 
        {
            //Database.Migrate();
        }

        public DbSet<Comment> TopicComent { get; set; }
        public DbSet<Discussion> TopicDiscussion { get; set; }


        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<User>(user =>
            {
                user.HasKey(u => u.Id);
            });

            modelBuilder.Entity<Comment>(tc => 
            {
                tc.HasKey(t => t.Id );
                tc.Property(t => t.Id).ValueGeneratedOnAdd();

                tc.Property(t => t.ComentMessage).HasMaxLength(500).IsRequired();

                tc.Property(t => t.DateComent).IsRequired();

                tc.HasOne(x=>x.User).WithMany(x=>x.Comments).HasForeignKey(x=>x.UserId).OnDelete(DeleteBehavior.Cascade);

                tc.HasOne(x => x.Discussion).WithMany(x => x.Comments).HasForeignKey(x=>x.DiscussionId).OnDelete(DeleteBehavior.Cascade);

            });

            modelBuilder.Entity<Discussion>(td => 
            {
                td.HasKey(t => t.Id );
                td.Property(t => t.Id).ValueGeneratedOnAdd();

                td.Property(t =>t.DiscussionMessage).HasMaxLength(1000).IsRequired();
                td.Property(t => t.IsDiscussed).HasDefaultValue(true);

                td.HasOne(x => x.Topic).WithMany(x => x.Discussions).HasForeignKey(x=>x.TopicId).OnDelete(DeleteBehavior.Cascade);
            });

            modelBuilder.Entity<DiscussionCategory>(dc => 
            {
                dc.HasKey(x => x.Id);
                dc.Property(x=>x.Id).ValueGeneratedOnAdd();

                dc.Property(x=>x.CategotyName).HasMaxLength(20);
            });

            modelBuilder.Entity<DiscussionTopic>(dt => 
            {
                dt.HasKey(x=>x.Id);
                dt.Property(x=>x.Id).ValueGeneratedOnAdd();

                dt.HasOne(x=>x.DiscussionCategory).WithMany(x=>x.Topics).HasForeignKey(x=>x.IdCategory).OnDelete(DeleteBehavior.Cascade);
            });
        }
    }
}
