﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Security.Claims;
using Forum.DAL.Entities;
using Microsoft.AspNetCore.Identity;
using System.Threading.Tasks;

namespace Forum.DAL.EF
{
    public static class RoleInitializer
    {
        public static async Task InitializeAsync(UserManager<User> userManager, RoleManager<IdentityRole> roleManager)
        {
            string rootEmail = "root@gmail.com";
            string rootPassword = "Root1";

            string adminEmail = "admin@gmail.com";
            string adminPassword = "Admin1";


            if (await roleManager.FindByNameAsync("Root") == null)
            {
                await roleManager.CreateAsync(new IdentityRole("Root"));
            }
            if (await roleManager.FindByNameAsync("Admin") == null)
            {
                await roleManager.CreateAsync(new IdentityRole("Admin"));
            }

            if (await userManager.FindByNameAsync(rootEmail) == null)
            {
                User root = new User { Email = rootEmail, UserName = rootEmail , FirstName = "Root", LastName="Root"};
                IdentityResult result = await userManager.CreateAsync(root, rootPassword);
                if (result.Succeeded)
                {
                    await userManager.AddToRoleAsync(root, "Root");
                }
            }

            if (await userManager.FindByNameAsync(adminEmail) == null)
            {
                User admin = new User { Email = adminEmail, UserName = adminEmail ,FirstName = "Admin", LastName = "Admin" };
                IdentityResult result = await userManager.CreateAsync(admin, adminPassword);
                if (result.Succeeded)
                {
                    await userManager.AddToRoleAsync(admin, "Admin");
                }
            }
        }
    }
}
