﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Forum.DAL.Entities;

namespace Forum.DAL.Entities
{
    public class Comment
    {
        public int Id {  get; set; }

        public string UserId { get; set; }
        public User User {  get; set; }

        public int DiscussionId { get; set; }
        public Discussion Discussion {  get; set; }

        public string ComentMessage { get; set; }
        public DateTime DateComent {  get; set; }

        public int CountLikesComment { get; set; }
        public int CountDislikesComment { get; set; }
    }
}
