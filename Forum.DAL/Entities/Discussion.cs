﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Forum.DAL.Entities
{
    public class Discussion
    {
        public int Id {  get; set; }

        public string UserId { get; set; }
        
        public string UserFullName {  get; set; }

        public string DiscussionMessage { get; set; }

        public DateTime DiscussionDate { get; set; }

        public bool IsDiscussed { get; set; }

        public int TopicId { get; set; }
        public DiscussionTopic Topic {  get; set; }

        public List<Comment> Comments {  get; set; } = new List<Comment>(); 
    }
}
