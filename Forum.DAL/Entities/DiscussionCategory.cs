﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Forum.DAL.Entities
{
    public class DiscussionCategory 
    {
        public int Id {  get; set; }

        public string CategotyName {  get; set; }

        public List<DiscussionTopic> Topics { get; set; } = new List<DiscussionTopic>();
    }
}
