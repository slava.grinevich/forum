﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Forum.DAL.Entities
{
    public class DiscussionTopic
    {
        public int Id {  get; set; }
        public string Topic {  get; set; }

        public int IdCategory { get; set; }
        public DiscussionCategory DiscussionCategory {  get; set; }

        public List<Discussion> Discussions { get; set; } = new List<Discussion>();
    }
}
