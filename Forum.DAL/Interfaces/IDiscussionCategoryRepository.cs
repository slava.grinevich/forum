﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Forum.DAL.Entities;

namespace Forum.DAL.Interfaces
{
    public interface IDiscussionCategoryRepository : IRepository<DiscussionCategory>
    {
        Task<IEnumerable<DiscussionTopic>> GetAllDiscussionTopicOfDiscussionCategoryAsync(int DiscussionCategoryId);
        Task<DiscussionCategory> GetDiscussionCategoryOfDiscussionTopicAsync(int TopicId);
    }
}
