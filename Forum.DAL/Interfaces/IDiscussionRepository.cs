﻿using Forum.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Forum.DAL.Interfaces
{
    public interface IDiscussionRepository : IRepository<Discussion>
    {
        Task<IEnumerable<Discussion>> GetAllDiscussionByUserIdAsync(string UserId);
        Task<IEnumerable<Discussion>> GetAllDiscussionOfDiscussionTopicAsync(int TopicId);
    }
}
