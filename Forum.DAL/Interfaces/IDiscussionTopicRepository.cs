﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Forum.DAL.Entities;
namespace Forum.DAL.Interfaces
{

    public interface IDiscussionTopicRepository : IRepository<DiscussionTopic>
    {
        Task<DiscussionTopic> GetDiscussionTopicByDiscussionIdAsync(int DiscussionId);
    }
}
