﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Forum.DAL.Entities;

namespace Forum.DAL.Interfaces
{
    public interface IUnitOfWork
    {
        public ICommentRepository Comments { get; }
        public IDiscussionCategoryRepository DiscussionCategory { get; }
        public IDiscussionRepository Discussion {  get; }
        public IDiscussionTopicRepository DiscussionTopic {  get; }
        public IUserRepository User {  get; }  
        

        Task SaveAsync();

        void Save();
    }
}
