﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Forum.DAL.Migrations
{
    public partial class comment1 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "TopicComentUser");

            migrationBuilder.AddColumn<int>(
                name: "DiscussionId",
                table: "TopicComent",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<string>(
                name: "UserId",
                table: "TopicComent",
                type: "nvarchar(450)",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_TopicComent_DiscussionId",
                table: "TopicComent",
                column: "DiscussionId");

            migrationBuilder.CreateIndex(
                name: "IX_TopicComent_UserId",
                table: "TopicComent",
                column: "UserId");

            migrationBuilder.AddForeignKey(
                name: "FK_TopicComent_AspNetUsers_UserId",
                table: "TopicComent",
                column: "UserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_TopicComent_TopicDiscussion_DiscussionId",
                table: "TopicComent",
                column: "DiscussionId",
                principalTable: "TopicDiscussion",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_TopicComent_AspNetUsers_UserId",
                table: "TopicComent");

            migrationBuilder.DropForeignKey(
                name: "FK_TopicComent_TopicDiscussion_DiscussionId",
                table: "TopicComent");

            migrationBuilder.DropIndex(
                name: "IX_TopicComent_DiscussionId",
                table: "TopicComent");

            migrationBuilder.DropIndex(
                name: "IX_TopicComent_UserId",
                table: "TopicComent");

            migrationBuilder.DropColumn(
                name: "DiscussionId",
                table: "TopicComent");

            migrationBuilder.DropColumn(
                name: "UserId",
                table: "TopicComent");

            migrationBuilder.CreateTable(
                name: "TopicComentUser",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    ComentId = table.Column<int>(type: "int", nullable: false),
                    DiscussionId = table.Column<int>(type: "int", nullable: false),
                    UserId = table.Column<string>(type: "nvarchar(450)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TopicComentUser", x => x.Id);
                    table.ForeignKey(
                        name: "FK_TopicComentUser_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_TopicComentUser_TopicComent_ComentId",
                        column: x => x.ComentId,
                        principalTable: "TopicComent",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_TopicComentUser_TopicDiscussion_DiscussionId",
                        column: x => x.DiscussionId,
                        principalTable: "TopicDiscussion",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_TopicComentUser_ComentId",
                table: "TopicComentUser",
                column: "ComentId");

            migrationBuilder.CreateIndex(
                name: "IX_TopicComentUser_DiscussionId",
                table: "TopicComentUser",
                column: "DiscussionId");

            migrationBuilder.CreateIndex(
                name: "IX_TopicComentUser_UserId",
                table: "TopicComentUser",
                column: "UserId");
        }
    }
}
