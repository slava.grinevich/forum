﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Forum.DAL.EF;
using Forum.DAL.Entities;
using Forum.DAL.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace Forum.DAL.Repositories
{
    public class CommentRepository : Repository<Comment>, ICommentRepository
    {
        public CommentRepository(AppDBContent context) : base(context)
        {
        }

        public async Task<IEnumerable<Comment>> GetAllCommentsByUsersIdAsync(string UserId)
        {
            return await DbSet.Include(x => x.User)
                .Where(c => c.UserId == UserId)
                .AsNoTracking()
                .ToListAsync();
        }

        public async Task<IEnumerable<Comment>> GetAllCommentsByDiscussionIdAsync(int DiscussionId)
        {
            return await DbSet
                .Where(c => c.DiscussionId == DiscussionId)
                .AsNoTracking()
                .ToListAsync();
        }
    }
}
