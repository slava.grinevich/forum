﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Forum.DAL.EF;
using Forum.DAL.Entities;
using Forum.DAL.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace Forum.DAL.Repositories
{
    public class DiscussionCategoryRepository : Repository<DiscussionCategory>, IDiscussionCategoryRepository
    {
        public DiscussionCategoryRepository(AppDBContent context) : base(context)
        {
        }

        public async Task<IEnumerable<DiscussionTopic>> GetAllDiscussionTopicOfDiscussionCategoryAsync(int DiscussionCategoryId)
        {
            return await DbSet.Include(x => x.Topics)
                .Where(x => x.Id == DiscussionCategoryId)
                .SelectMany(x => x.Topics)
                .AsNoTracking()
                .ToListAsync();
        }

        public async Task<DiscussionCategory> GetDiscussionCategoryOfDiscussionTopicAsync(int TopicId)
        {
            return await DbSet.Include(x => x.Topics)
                .Where(x => x.Topics.Any(c => c.Id == TopicId))
                .AsNoTracking()
                .FirstOrDefaultAsync();
        }

    }
}
