﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Forum.DAL.EF;
using Forum.DAL.Entities;
using Forum.DAL.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace Forum.DAL.Repositories
{
    public class DiscussionRepository : Repository<Discussion>, IDiscussionRepository
    {
        public DiscussionRepository(AppDBContent context) : base(context)
        {
        }

        public async Task<IEnumerable<Discussion>> GetAllDiscussionByUserIdAsync(string UserId)
        {
            
            return await DbSet.Include(x => x.Comments)
                .Where(x => x.UserId == UserId)
                .AsNoTracking()
                .ToListAsync();
        }

        public async Task<IEnumerable<Discussion>> GetAllDiscussionOfDiscussionTopicAsync(int TopicId)
        {
            return await DbSet.Include(x => x.Topic)
                .Where(x=>x.Topic.Id == TopicId)
                .AsNoTracking()
                .ToListAsync();
        }
    }
}
