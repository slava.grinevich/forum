﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Forum.DAL.EF;
using Forum.DAL.Entities;
using Forum.DAL.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace Forum.DAL.Repositories
{
    public class DiscussionTopicRepository : Repository<DiscussionTopic>, IDiscussionTopicRepository
    {
        public DiscussionTopicRepository(AppDBContent context) : base(context)
        {
        }

        public async Task<IEnumerable<Discussion>> GetAllDiscussionOfDiscussionTopicAsync(int TopicId)
        {
            return await DbSet
                .Include(x => x.Discussions).SelectMany(x=>x.Discussions)
                .Where(x => x.TopicId == TopicId)
                .AsNoTracking()
                .ToListAsync();
        }
        public async Task<DiscussionTopic> GetDiscussionTopicByDiscussionIdAsync(int DiscussionId)
        {
            return await DbSet
                .Include(x => x.Discussions)
                .Where(x => x.Discussions.Any(c => c.Id == DiscussionId))
                .AsNoTracking()
                .FirstOrDefaultAsync();
        }
    }
}
