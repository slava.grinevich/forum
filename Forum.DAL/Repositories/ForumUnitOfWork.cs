﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Forum.DAL.Entities;
using Forum.DAL.Interfaces;
using Forum.DAL.EF;

namespace Forum.DAL.Repositories
{
    public class ForumUnitOfWork : IUnitOfWork
    {
        private readonly AppDBContent _db;

        public ForumUnitOfWork(
            AppDBContent context,
            ICommentRepository commentRepository,
            IDiscussionCategoryRepository discussionCategoryRepository,
            IDiscussionRepository discussionRepository,
            IDiscussionTopicRepository discussionTopicRepository,
            IUserRepository userRepository)
        {
            _db = context;
            Comments = commentRepository;
            DiscussionCategory = discussionCategoryRepository;
            Discussion = discussionRepository;
            DiscussionTopic = discussionTopicRepository;
            User = userRepository;
        }


        public ICommentRepository Comments { get; }
        public IDiscussionCategoryRepository DiscussionCategory { get; }
        public IDiscussionRepository Discussion { get; }
        public IDiscussionTopicRepository DiscussionTopic { get; }
        public IUserRepository User { get; }

        public async Task SaveAsync()
        {
            await _db.SaveChangesAsync();
        }

        public void Save()
        {
            _db.SaveChanges();
        }
    }
}
