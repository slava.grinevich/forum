﻿using Forum.DAL.EF;
using Forum.DAL.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Forum.DAL.Repositories
{
    public abstract class Repository<T> : IRepository<T> where T : class
    {
        protected readonly AppDBContent Context;
        protected readonly DbSet<T> DbSet;

        public Repository(AppDBContent context)
        {
            Context = context;
            DbSet = context.Set<T>();
        }

        public virtual async Task<IEnumerable<T>> GetAllAsync()
        {
            return await DbSet
                .AsNoTracking()
                .ToListAsync();
        }

        public virtual async Task<T> GetAsync(int id)
        {
            return await DbSet.FindAsync(id);
        }

        public virtual async Task CreateAsync(T item)
        {
            await DbSet.AddAsync(item);
        }

        public virtual void Update(T item)
        {
            DbSet.Update(item);
        }

        public virtual void Delete(T item)
        {
            DbSet.Remove(item);
        }

        public virtual void Detach(T item)
        {
            Context.Entry(item).State = EntityState.Detached;
        }

        public virtual void Modified(T item)
        {
            Context.Entry(item).State = EntityState.Modified;
        }
    }
}
