﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Forum.BLL.DTO;
using Forum.DAL.Entities;
using Microsoft.AspNetCore.Identity;
using Forum.WEB.ViewModel;
using Microsoft.AspNetCore.Http;
using System.Web;
using System;

namespace Forum.WEB.Controllers
{
    /// <summary>
    ///     Controller that performs authentication and authorization
    /// </summary>
    public class AccountController : Controller
    {
        private readonly UserManager<User> _userManager;
        private readonly SignInManager<User> _signInManager;


        /// <summary>
        ///     Method that creates the account controller (login and registration)
        /// </summary>
        /// <param name="userManager">
        ///     User manager to manage them
        /// </param>
        /// <param name="signInManager">
        ///     Login manager that determines whether the person entered the correct data
        /// </param>
        public AccountController(UserManager<User> userManager, SignInManager<User> signInManager)
        {
            _userManager = userManager;
            _signInManager = signInManager;
        }

        /// <summary>
        ///     Method that renders page with registration fields
        /// </summary>
        /// <returns>
        ///     Page with registration fields
        /// </returns>
        [HttpGet]
        public IActionResult Register()
        {
            return View();
        }


        /// <summary>
        ///     Method that process registration for provided registration data
        /// </summary>
        /// <param name="RegisterModel">
        ///     Registration data from form
        /// </param>
        /// <returns>
        ///     Main page with all games
        /// </returns>
        [HttpPost]
        public async Task<IActionResult> Register(RegisterViewModel RegisterModel)
        {
            if (ModelState.IsValid)
            {
                User user = new User { 
                    FirstName = RegisterModel.FirstName,
                    LastName = RegisterModel.LastName,
                    Email = RegisterModel.Email, 
                    UserName = RegisterModel.Email
                };
                var result = await _userManager.CreateAsync(user, RegisterModel.Password);
                if (result.Succeeded)
                {
                    await _signInManager.SignInAsync(user, false);
                    return RedirectToAction("Index", "Home");
                }
                else
                {
                    foreach (var error in result.Errors)
                    {
                        ModelState.AddModelError(string.Empty, error.Description);
                    }
                }
            }
            return View(RegisterModel);
        }

        /// <summary>
        ///     Method that renders page with registration fields
        /// </summary>
        /// <returns>
        ///     Page with logins fields
        /// </returns>
        [HttpGet]
        public IActionResult Login()
        {
            LoginViewModel loginModel = new LoginViewModel();
            loginModel.ReturnUrl = Request.Headers["Referer"].ToString();

            return View(loginModel);
        }


        /// <summary>
        ///     Method that process login for provided user data
        /// </summary>
        /// <param name="LoginModel">
        ///     Login data from form
        /// </param>
        /// <returns>
        ///     Redirect to main page
        /// </returns>
        [HttpPost]
        public async Task<IActionResult> Login(LoginViewModel LoginModel)
        {
            if (ModelState.IsValid)
            {
                var result =
                    await _signInManager.PasswordSignInAsync(LoginModel.Email, LoginModel.Password, LoginModel.RememberMe, false);
                if (result.Succeeded)
                {
                    if (!string.IsNullOrEmpty(LoginModel.ReturnUrl) && Url.IsLocalUrl(LoginModel.ReturnUrl))
                    {
                        return Redirect(LoginModel.ReturnUrl);
                    }
                    else
                    {
                        return RedirectToAction("Index", "Home");
                    }
                }
                else
                {
                    ModelState.AddModelError("", "Неправильный логин и (или) пароль");
                }
            }
            return View(LoginModel);
        }


        /// <summary>
        ///     Logs out current user
        /// </summary>
        /// <returns>
        ///     redirect to main page
        /// </returns>
        [HttpPost]
        public async Task<IActionResult> Logout()
        {
            await _signInManager.SignOutAsync();
            return RedirectToAction("StartPage", "Home");
        }
    }
}