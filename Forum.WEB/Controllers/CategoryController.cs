﻿using AutoMapper;
using Forum.BLL.Interfaces;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using Forum.WEB.ViewModel;
using Forum.BLL.DTO;
using System.Collections.Generic;
using System;
using System.Linq;

namespace Forum.WEB.Controllers
{
    /// <summary>
    ///     Controller that performs display of all categories and their discussion topics
    /// </summary>
    public class CategoryController : Controller
    {
        private readonly IDiscussionCategoryService _discussionCategoryService;
        private readonly IMapper _mapper;


        /// <summary>
        ///     Method that creates the category controller (add category and view all category)
        /// </summary>
        /// <param name="mapper">
        ///     Mapper to create projections of one model in other model
        /// </param>
        /// <param name="discussionCategoryService">
        ///     Interface for working with categories in the database indirectly
        /// </param>
        public CategoryController(
            IMapper mapper,
            IDiscussionCategoryService discussionCategoryService)
        {
            _mapper = mapper;
            _discussionCategoryService = discussionCategoryService;
        }

        /// <summary>
        ///     Method that renders page with one categoty and its thems
        /// </summary>
        /// <param name="categoryId">
        ///     Id category from which we want to see topics 
        /// </param>
        /// <returns>
        ///     Page with one categoty and its themes
        /// </returns>
        [HttpGet]
        public IActionResult Index(string categoryId)
        {
            DiscussionCategoryViewModel categotyView = 
                _mapper.Map<DiscussionCategoryViewModel>(_discussionCategoryService.GetAsync(Convert.ToInt32(categoryId)).Result);
            categotyView.Topics =
                _mapper.Map<List<DiscussionTopicViewModel>>(_discussionCategoryService.GetAllDiscussionTopicOfDiscussionCategoryAsync(Convert.ToInt32(categoryId)).Result);

            return View(categotyView);
        }


        /// <summary>
        ///     Method that renders page with add new category fields
        /// </summary>
        /// <returns>
        ///     Page with add new category fields
        /// </returns>
        [HttpGet]
        public IActionResult CreatCategory()
        {
            return View();
        }


        /// <summary>
        ///     Method that process creat category for provided category data
        /// </summary>
        /// <param name="CategoryModel">
        ///     Category data from form
        /// </param>
        /// <returns>
        ///     Main page with all categories
        /// </returns>
        [HttpPost]
        public async Task<IActionResult> CreatCategory(DiscussionCategoryViewModel CategoryModel)
        {
            var checkCategory = await _discussionCategoryService.GetAllAsync();
            if (checkCategory.Where(x => x.CategotyName.ToLower() == CategoryModel.CategotyName.ToLower()).Count() > 0)
            {
                ModelState.AddModelError("", "Такая категория уже есть");
                return View(CategoryModel);
            }

            await _discussionCategoryService.CreateAsync(_mapper.Map<DiscussionCategoryDTO>(CategoryModel));
            return RedirectToAction("Index", "Home");
        }


        /// <summary>
        ///     Method that process dell topic for provided topic data
        /// </summary>
        /// <param name="categoryId">
        ///     Id category to wich we retern after delete
        /// </param>
        /// <returns>
        ///     Main page with all topics
        /// </returns>
        [HttpPost]
        public async Task<IActionResult> DeleteCategory(string categoryId)
        {
            await _discussionCategoryService.Delete(Convert.ToInt32(categoryId));
            return RedirectToAction("Index", "Home");
        }


        /// <summary>
        ///     Method that renders page with edit category fields
        /// </summary>
        /// <returns>
        ///     Page with edit category fields
        /// </returns>
        [HttpGet]
        public IActionResult EditCategory(string categoryId)
        {
            return View(new DiscussionCategoryViewModel { Id = Convert.ToInt32(categoryId)});
        }


        /// <summary>
        ///     Method that process edit category for provided category data
        /// </summary>
        /// <param name="CategoryModel">
        ///     Category data from form
        /// </param>
        /// <returns>
        ///     Main page with all categories
        /// </returns>
        [HttpPost]
        public async Task<IActionResult> EditCategory(DiscussionCategoryViewModel CategoryModel)
        {
            var checkCategory = await _discussionCategoryService.GetAllAsync();
            if (checkCategory.Where(x => x.CategotyName.ToLower() == CategoryModel.CategotyName.ToLower()).Count() > 0)
            {
                ModelState.AddModelError("", "Такая категория уже есть");
                return View(CategoryModel);
            }

            await _discussionCategoryService.Update(_mapper.Map<DiscussionCategoryDTO>(CategoryModel));
            return RedirectToAction("Index", "Home");
        }
    }
}
