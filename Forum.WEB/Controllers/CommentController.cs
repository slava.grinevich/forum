﻿using AutoMapper;
using Forum.BLL.DTO;
using Forum.BLL.Interfaces;
using Forum.DAL.Entities;
using Forum.WEB.ViewModel;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;

namespace Forum.WEB.Controllers
{
    /// <summary>
    ///     Controller that performs add new comments and add likes or dislikes for one
    /// </summary>
    public class CommentController : Controller
    {
        private readonly ICommentService _commentService;
        private readonly IMapper _mapper;
        private readonly IUserService _userService;
        private readonly UserManager<User> _userManager;


        /// <summary>
        ///     Method that creates the category controller (add category and view all category)
        /// </summary>
        /// <param name="mapper">
        ///     Mapper to create projections of one model in other model
        /// </param>
        /// <param name="commentService">
        ///     Interface for working with comments in the database indirectly
        /// </param>
        /// <param name="userService">
        ///     Interface for working with users in the database indirectly
        /// </param>
        public CommentController(
            IMapper mapper,
            ICommentService commentService,
            IUserService userService,
            UserManager<User> userManager)
        {
            _mapper = mapper;
            _commentService = commentService;
            _userService = userService;
            _userManager = userManager;
        }

        /// <summary>
        ///     Method that create new discussion with userId
        /// </summary>
        /// <param name="discussionId">
        ///     Topic id to relate to discussion
        /// </param>
        /// <param name="userId">
        ///     Id category from which we want to see topics 
        /// </param>
        /// <returns>
        ///     Page with one categoty and its themes
        /// </returns>
        [HttpPost]
        public async Task<IActionResult> CreatComment(string discussionId,string userName, string comentMessage)
        {
            if (comentMessage==null || comentMessage.Length < 5 || comentMessage.Length > 500)
            {
                //ModelState.AddModelError("", "Неправильная длина коментария");
                return RedirectToAction("Index", "Discussion", new { DiscussionId = discussionId });
            }
            
            User findUser = await _userManager.FindByNameAsync(userName);
            CommentDTO commentView = new CommentDTO()
            {
                ComentMessage = comentMessage,
                UserId = findUser.Id,
                DiscussionId = Convert.ToInt32(discussionId),
                DateComent = DateTime.Now,
                CountDislikesComment = 5,
                CountLikesComment = 5,
                UserFullName = User.Identity.Name,
            };

            await _commentService.CreateAsync(commentView);
            return RedirectToAction("Index", "Discussion", new { DiscussionId = discussionId });
        }


        /// <summary>
        ///     Method that create new discussion with userId
        /// </summary>
        /// <param name="discussionId">
        ///     Topic id to relate to discussion
        /// </param>
        /// <param name="commentId">
        ///     Id comment from which we want to add like
        /// </param>
        /// <returns>
        ///     Page with one categoty and its themes
        /// </returns>
        [HttpPost]
        public async Task<IActionResult> AddLike(string discussionId, string commentId)
        {
            await _commentService.AddLike(Convert.ToInt32(commentId));
            await _userService.CountAllRatingCommentsByUserIdAsync(_commentService.GetAsync(Convert.ToInt32(commentId)).Result.UserId);
            return RedirectToAction("Index", "Discussion", new { DiscussionId = discussionId });
        }


        /// <summary>
        ///     Method that create new discussion with userId
        /// </summary>
        /// <param name="discussionId">
        ///     Topic id to relate to discussion
        /// </param>
        /// <param name="commentId">
        ///     Id comment from which we want to add dislike
        /// </param>
        /// <returns>
        ///     Page with one categoty and its themes
        /// </returns>
        [HttpPost]
        public async Task<IActionResult> AddDislike(string discussionId, string commentId)
        {
            await _commentService.AddDislike(Convert.ToInt32(commentId));
            await _userService.CountAllRatingCommentsByUserIdAsync(_commentService.GetAsync(Convert.ToInt32(commentId)).Result.UserId);
            return RedirectToAction("Index", "Discussion", new { DiscussionId = discussionId });
        }



        /// <summary>
        ///     Method that process dell topic for provided topic data
        /// </summary>
        /// <param name="discussionId">
        ///     Topic id to relate to discussion
        /// </param>
        /// <param name="commentId">
        ///     Id comment to wich we retern after delete
        /// </param>
        /// <returns>
        ///     Main page with all topics
        /// </returns>
        [HttpPost]
        public async Task<IActionResult> DeleteComment(string discussionId, string commentId)
        {
            var userId = await _commentService.GetAsync(Convert.ToInt32(commentId));
            
            await _commentService.Delete(Convert.ToInt32(commentId));

            await _userService.CountAllRatingCommentsByUserIdAsync(userId.UserId);

            return RedirectToAction("Index", "Discussion", new { DiscussionId = discussionId });
        }

    }
}
