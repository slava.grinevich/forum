﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Forum.BLL.DTO;
using Forum.DAL.Entities;
using Microsoft.AspNetCore.Identity;
using Forum.WEB.ViewModel;
using AutoMapper;
using Forum.BLL.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Forum.WEB.Controllers
{
    /// <summary>
    ///     Controller that performs display of all discussion of topic
    /// </summary>
    public class DiscussionController : Controller
    {
        private readonly IDiscussionService _discussionService;
        private readonly IMapper _mapper;
        private readonly UserManager<User> _userManager;
        private readonly ICommentService _commentService;


        /// <summary>
        ///     Method that creates the discussion controller (add discussion and view all discussions)
        /// </summary>
        /// <param name="mapper">
        ///     Mapper to create projections of one model in other model
        /// </param>
        /// <param name="discussionService">
        ///     Interface for working with discussion in the database indirectly
        /// </param>
        /// <param name="userManager">
        ///     User manager to manage them
        /// </param>
        /// <param name="commentService">
        ///     Interface for working with comment in the database indirectly
        /// </param>
        public DiscussionController(
            IMapper mapper,
            IDiscussionService discussionService,
            UserManager<User> userManager,
            ICommentService commentService)
        {
            _mapper = mapper;
            _discussionService = discussionService;
            _userManager = userManager;
            _commentService = commentService;
        }

        /// <summary>
        ///     Method that renders page with discussion and comments
        /// </summary>
        /// <param name="DiscussionId">
        ///     Id discussion with we wand commented or read
        /// </param>
        /// <returns>
        ///     Page with discussion and comments
        /// </returns>
        [HttpGet]
        public async Task<IActionResult> Index(string DiscussionId)
        {
            DiscussionViewModel discussionViewModel = 
                _mapper.Map<DiscussionViewModel>(_discussionService.GetAsync(Convert.ToInt32(DiscussionId)).Result);

            discussionViewModel.Comments = 
                _mapper.Map<List<CommentViewModel>>(_commentService.GetAllCommentsByDiscussionIdAsync(Convert.ToInt32(DiscussionId)).Result);

            foreach (var i in discussionViewModel.Comments)
            {
                i.User = _mapper.Map<UserDTO>( await _userManager.FindByIdAsync(i.UserId));
                i.UserFullName = i.User.UserName;
            }

            return View(discussionViewModel);
        }

        /// <summary>
        ///     Method that create new discussion with userId
        /// </summary>
        /// <param name="topicId">
        ///     Topic id to relate to discussion
        /// </param>
        /// <param name="userName">
        ///     Id category from which we want to see topics 
        /// </param>
        /// <returns>
        ///     Page with one categoty and its themes
        /// </returns>
        //[HttpGet]
        //public IActionResult CreatDiscussion(string topicId, string userName)
        //{
        //    User FindUser = _userManager.FindByNameAsync(userName).Result;
        //    DiscussionViewModel discussion = new DiscussionViewModel() 
        //    {
        //        TopicId = Convert.ToInt32(topicId),
        //        DiscussionDate= DateTime.Now,
        //        IsDiscussed= true,
        //        UserFullName= FindUser.FirstName.ToString() + " " + FindUser.LastName.ToString(),
        //        UserId = FindUser.Id
        //    };
        //    return View(discussion);
        //}

        /// <summary>
        ///     Method that create new discussion with userId
        /// </summary>
        /// <param name="allParams">
        ///     Topic id to relate to discussion
        /// </param>
        /// <returns>
        ///     Page with one categoty and its themes
        /// </returns>
        [HttpGet]
        public IActionResult CreatDiscussion(AddDiscussionTopicViewModel discussionView)
        {
            string topicId = discussionView.TopicId;
            string userName = User.Identity.Name;

            User FindUser = _userManager.FindByNameAsync(userName).Result;
            DiscussionViewModel discussion = new DiscussionViewModel()
            {
                TopicId = Convert.ToInt32(topicId),
                DiscussionDate = DateTime.Now,
                IsDiscussed = true,
                UserFullName = FindUser.FirstName.ToString() + " " + FindUser.LastName.ToString(),
                UserId = FindUser.Id
            };
            return View(discussion);
        }

        /// <summary>
        ///     Method that process creat discussion for provided discussion data
        /// </summary>
        /// <param name="DiscussionModel">
        ///     Discussion data from form
        /// </param>
        /// <returns>
        ///     Main page with all discussion in this topic
        /// </returns>
        [HttpPost]
        public async Task<IActionResult> CreatDiscussion(DiscussionViewModel DiscussionModel)
        {
            var checkDiscussion = await _discussionService.GetAllAsync();
            if (checkDiscussion.Where(x => x.DiscussionMessage.ToLower() == DiscussionModel.DiscussionMessage.ToLower()).Count() > 0)
            {
                ModelState.AddModelError("", "Такое обсуждение уже есть");
                return View(DiscussionModel);
            }

            DiscussionDTO discussion = _mapper.Map<DiscussionDTO>(DiscussionModel);

            await _discussionService.CreateAsync(discussion);

            return RedirectToAction("Index", "Topic", new { topicId = discussion.TopicId });
        }

        /// <summary>
        ///     Method that process dell discussion for provided topic data
        /// </summary>
        /// <param name="discussionId">
        ///     Id discussion wich we wand delete
        /// </param>
        /// <param name="topicId">
        ///     Id topic to wich we retern after delete
        /// </param>
        /// <returns>
        ///     Main page with all discussion with one topic
        /// </returns>
        [HttpPost]
        public async Task<IActionResult> DeleteDiscussion(string discussionId, string topicId)
        {
            await _discussionService.Delete(Convert.ToInt32(discussionId));
            return RedirectToAction("Index", "Topic", new { topicId = topicId });
        }

        /// <summary>
        ///     Method that renders page with edit discussion fields
        /// </summary>
        /// <param name="discussionId">
        ///     Id discussion with we want edit
        /// </param>
        /// <returns>
        ///     Page with edit discussion fields
        /// </returns>
        [HttpGet]
        public async Task<IActionResult> EditDiscussion(string discussionId)
        {
            var tmp =  await _discussionService.GetAsync(Convert.ToInt32(discussionId));

            var DiscussionView = _mapper.Map<DiscussionViewModel>(tmp);
            DiscussionView.Id = tmp.Id;

            return View(DiscussionView);
        }


        /// <summary>
        ///     Method that process edit category for provided discussion data
        /// </summary>
        /// <param name="DiscussionModel">
        ///     Discussion data from form
        /// </param>
        /// <returns>
        ///     Main page with all discussion with one topic
        /// </returns>
        [HttpPost]
        public  async Task<IActionResult> EditDiscussion(DiscussionViewModel DiscussionModel)
        {
            var checkDiscussion = await _discussionService.GetAllAsync();
            if (checkDiscussion.Where(x => x.DiscussionMessage.ToLower() == DiscussionModel.DiscussionMessage.ToLower()).Count() > 0)
            {
                ModelState.AddModelError("", "Такое обсуждение уже есть");
                return View(DiscussionModel);
            }

            await _discussionService.Update(_mapper.Map<DiscussionDTO>(DiscussionModel));

            return RedirectToAction("Index", "Topic", new { topicId = DiscussionModel.TopicId });
        }

    }
}
