﻿using AutoMapper;
using Forum.BLL.Interfaces;
using Forum.BLL.Services;
using Forum.WEB.Models;
using Forum.WEB.ViewModel;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace Forum.WEB.Controllers
{
    /// <summary>
    ///     Controller that performs display of all categories and topics
    /// </summary>
    public class HomeController : Controller
    {
        private readonly IDiscussionCategoryService _discussionCategoryService;
        private readonly IMapper _mapper;

        /// <summary>
        ///     Method that creates the discussion controller (add discussion and view all discussions)
        /// </summary>
        /// <param name="mapper">
        ///     Mapper to create projections of one model in other model
        /// </param>
        /// <param name="discussionCategoryService">
        ///     Interface for working with categories in the database indirectly
        /// </param>
        public HomeController(IMapper mapper,
            IDiscussionCategoryService discussionCategoryService)
        {
            _mapper = mapper;
            _discussionCategoryService = discussionCategoryService;
        }


        /// <summary>
        ///     Method that renders page with categories and topics
        /// </summary>
        /// <returns>
        ///     Page with categories and topics
        /// </returns>
        public IActionResult Index()
        {
            IEnumerable<DiscussionCategoryViewModel> Category = 
                _mapper.Map<IEnumerable<DiscussionCategoryViewModel>>(_discussionCategoryService.GetAllAsync().Result);
            foreach (var item in Category)
            {
                item.Topics = _mapper.Map<List<DiscussionTopicViewModel>>(_discussionCategoryService.GetAllDiscussionTopicOfDiscussionCategoryAsync(item.Id).Result);
                item.CountAllDiscussion = _discussionCategoryService.CountAllDiscussionOfCategoryId(item.Id).Result;
            }

            StartViewModel startView = new StartViewModel { AllCategory = Category };

            return View(startView);
        }


        public IActionResult StartPage()
        {
            return View();
        }


        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
