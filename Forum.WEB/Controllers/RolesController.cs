﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Forum.WEB.Models;
using Forum.WEB.ViewModel;
using Forum.DAL.Entities;
using Microsoft.AspNetCore.Authorization;

namespace Forum.WEB.Controllers
{
    /// <summary>
    ///     Controller that performs display of all roles and users with their roles
    /// </summary>
    [Authorize(Roles = "Root")]
    public class RolesController : Controller
    {
        RoleManager<IdentityRole> _roleManager;
        UserManager<User> _userManager;

        /// <summary>
        ///     Method that creates the roles controller (add role, define a person's role)
        /// </summary>
        /// <param name="roleManager">
        ///     Role manager to manage them
        /// </param>
        /// <param name="userManager">
        ///     User manager to manage them
        /// </param>
        public RolesController(RoleManager<IdentityRole> roleManager, UserManager<User> userManager)
        {
            _roleManager = roleManager;
            _userManager = userManager;
        }

        /// <summary>
        ///     Method that renders page with all roles
        /// </summary>
        /// <returns>
        ///     Page with all roles
        /// </returns>
        public IActionResult Index() => View(_roleManager.Roles.ToList());

        /// <summary>
        ///     Method that renders page with add new roles fields
        /// </summary>
        /// <returns>
        ///     Page with roles fields
        /// </returns>
        public IActionResult Create() => View();


        /// <summary>
        ///     Method that process add new role for provided role name data
        /// </summary>
        /// <param name="name">
        ///     Registration data from form
        /// </param>
        /// <returns>
        ///     Page with list of all roles
        /// </returns>
        [HttpPost]
        public async Task<IActionResult> Create(string name)
        {
            if (!string.IsNullOrEmpty(name))
            {
                IdentityResult result = await _roleManager.CreateAsync(new IdentityRole(name));
                if (result.Succeeded)
                {
                    return RedirectToAction("Index");
                }
                else
                {
                    foreach (var error in result.Errors)
                    {
                        ModelState.AddModelError(string.Empty, error.Description);
                    }
                }
            }
            return View(name);
        }

        /// <summary>
        ///     Method that deleting a role
        /// </summary>
        /// <param name="id">
        ///     Id role with we want deleted
        /// </param>
        /// <returns>
        ///     Page with list of all roles without one
        /// </returns>
        [HttpPost]
        public async Task<IActionResult> Delete(string id)
        {
            IdentityRole role = await _roleManager.FindByIdAsync(id);
            if (role != null)
            {
                IdentityResult result = await _roleManager.DeleteAsync(role);
            }
            return RedirectToAction("Index");
        }


        /// <summary>
        ///     Method that renders page with all users with their roles
        /// </summary>
        /// <returns>
        ///     Page with all users with their roles
        /// </returns>
        public IActionResult UserList() => View(_userManager.Users.ToList());

        /// <summary>
        ///     Method that process editing user role for provided user Id data
        /// </summary>
        /// <param name="userId">
        ///     Id users with we want change role
        /// </param>
        /// <returns>
        ///     Page with editing user role
        /// </returns>
        public async Task<IActionResult> Edit(string userId)
        {
            User user = await _userManager.FindByIdAsync(userId);
            if (user != null)
            {
                var userRoles = await _userManager.GetRolesAsync(user);
                var allRoles = _roleManager.Roles.ToList();
                ChangeRoleViewModel model = new ChangeRoleViewModel
                {
                    UserId = user.Id,
                    UserEmail = user.Email,
                    UserRoles = userRoles,
                    AllRoles = allRoles
                };
                return View(model);
            }

            return NotFound();
        }

        /// <summary>
        ///     Method that editing user role for provided user Id data
        /// </summary>
        /// <param name="userId">
        ///     Id users with we want change role
        /// </param>
        /// <param name="roles">
        ///     list of user roles 
        /// </param>
        /// <returns>
        ///     Page with all users with their roles
        /// </returns>
        [HttpPost]
        public async Task<IActionResult> Edit(string userId, List<string> roles)
        {
            User user = await _userManager.FindByIdAsync(userId);
            if (user != null)
            {
                
                var userRoles = await _userManager.GetRolesAsync(user);
                
                var allRoles = _roleManager.Roles.ToList();
                
                var addedRoles = roles.Except(userRoles);
                
                var removedRoles = userRoles.Except(roles);

                await _userManager.AddToRolesAsync(user, addedRoles);

                await _userManager.RemoveFromRolesAsync(user, removedRoles);

                return RedirectToAction("UserList");
            }

            return NotFound();
        }
    }
}