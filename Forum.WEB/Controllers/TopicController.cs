﻿using AutoMapper;
using Forum.BLL.Interfaces;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using Forum.WEB.ViewModel;
using Forum.BLL.DTO;
using System.Collections.Generic;
using System;
using System.Linq;

namespace Forum.WEB.Controllers
{
    /// <summary>
    ///     Controller that performs display of all topics and their discussions
    /// </summary>
    public class TopicController : Controller
    {
        private readonly IDiscussionTopicService _discussionTopicService;
        private readonly IDiscussionService _discussionService;
        private readonly IMapper _mapper;

        /// <summary>
        ///     Method that creates the ropic controller (add topic, add discussion to topic)
        /// </summary>
        /// <param name="mapper">
        ///     Mapper to create projections of one model in other model
        /// </param>
        /// <param name="discussionService">
        ///     Interface for working with discussion in the database indirectly
        /// </param>
        /// <param name="discussionTopicService">
        ///     Interface for working with topics in the database indirectly
        /// </param>
        public TopicController(
            IMapper mapper,
            IDiscussionService discussionService,
            IDiscussionTopicService discussionTopicService)
        {
            _mapper = mapper;
            _discussionService = discussionService;
            _discussionTopicService = discussionTopicService;
        }


        /// <summary>
        ///     Method that renders page with discussion on the chosen topic
        /// </summary>
        /// <param name="topicId">
        ///     Id topic where do we want to see all discussion
        /// </param>
        /// <returns>
        ///     Page with discussion on the chosen topic
        /// </returns>
        [HttpGet]
        public IActionResult Index(string topicId)
        {
            DiscussionTopicViewModel discussionTopic =
                _mapper.Map<DiscussionTopicViewModel>(_discussionTopicService.GetAsync(Convert.ToInt32(topicId)).Result);

            discussionTopic.Discussions = 
                _mapper.Map<List<DiscussionViewModel>>(_discussionService.GetAllDiscussionOfDiscussionTopicAsync(Convert.ToInt32(topicId)).Result);

            AddDiscussionTopicViewModel addDiscussionTopicView = new AddDiscussionTopicViewModel
            {
                discussionTopicViewModel = discussionTopic
            };

            return View(addDiscussionTopicView);
        }


        /// <summary>
        ///     Method that renders page with add new topic in the chosen category
        /// </summary>
        /// <param name="categoryId">
        ///     Id categoty where do we want to add topic
        /// </param>
        /// <returns>
        ///     Page with add new category fields
        /// </returns>
        [HttpGet]
        public IActionResult CreatTopic(string categoryId)
        {
            return View(new DiscussionTopicViewModel() {IdCategory = Convert.ToInt32(categoryId) });
        }

        /// <summary>
        ///     Method that process creat topic for provided topic data
        /// </summary>
        /// <param name="TopicModel">
        ///     Topic data from form
        /// </param>
        /// <returns>
        ///     Main page with all topics
        /// </returns>
        [HttpPost]
        public async Task<IActionResult> CreatTopic(DiscussionTopicViewModel TopicModel)
        {
            var checkTopic = await _discussionTopicService.GetAllAsync();
            if (checkTopic.Where(x => x.Topic.ToLower() == TopicModel.Topic.ToLower()).Count() > 0)
            {
                ModelState.AddModelError("", "Такая тема уже есть");
                return View(TopicModel);
            }

            DiscussionTopicDTO category = new DiscussionTopicDTO()
            {
                Topic = TopicModel.Topic,
                IdCategory = TopicModel.IdCategory
            };

            await _discussionTopicService.CreateAsync(category);

            return RedirectToAction("Index", "Category", new { categoryId = TopicModel.IdCategory.ToString()});
        }

        /// <summary>
        ///     Method that process dell topic for provided topic data
        /// </summary>
        /// <param name="topicId">
        ///     Id topic wich we wand delete
        /// </param>
        /// <param name="categoryId">
        ///     Id category to wich we retern after delete
        /// </param>
        /// <returns>
        ///     Main page with all topics
        /// </returns>
        [HttpPost]
        public async Task<IActionResult> DeleteTopic(string topicId, string categoryId)
        {
            await _discussionTopicService.Delete(Convert.ToInt32(topicId));
            return RedirectToAction("Index", "Category", new { categoryId = categoryId });
        }


        /// <summary>
        ///     Method that renders page with edit topic fields
        /// </summary>
        /// <returns>
        ///     Page with edit topic fields
        /// </returns>
        [HttpGet]
        public IActionResult EditTopic(string topicId)  
        {
            var tmp = _discussionTopicService.GetAsync(Convert.ToInt32(topicId)).Result;
            return View(_mapper.Map<DiscussionTopicViewModel>(tmp));
        }


        /// <summary>
        ///     Method that process edit category for provided topic data
        /// </summary>
        /// <param name="TopicModel">
        ///     Topic data from form
        /// </param>
        /// <returns>
        ///     Main page with all topic with one category
        /// </returns>
        [HttpPost]
        public async Task<IActionResult> EditTopic(DiscussionTopicViewModel TopicModel)
        {
            var checkRole = await _discussionTopicService.GetAllAsync();
            if (checkRole.Where(x => x.Topic.ToLower() == TopicModel.Topic.ToLower()).Count() > 0)
            {
                ModelState.AddModelError("", "Такая тема уже есть");
                return View(TopicModel);
            }

            await _discussionTopicService.Update(_mapper.Map<DiscussionTopicDTO>(TopicModel));

            return RedirectToAction("Index", "Category", new { categoryId = TopicModel.IdCategory });
        }
    }
}
