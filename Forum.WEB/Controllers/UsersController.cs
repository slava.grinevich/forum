﻿using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Identity;
using Forum.DAL.Entities;
using Forum.WEB.ViewModel;
using Forum.DAL.Interfaces;
using AutoMapper;
using Forum.BLL.Services;
using Microsoft.AspNetCore.Authorization;
using Forum.BLL.Interfaces;

namespace Forum.WEB.Controllers
{
    /// <summary>
    ///     Controer that performs display of all users
    /// </summary>
    [Authorize(Roles = "Admin,Root")]
    public class UsersController : Controller
    {
        public IMapper _mapper;
        public IUserService _userService;
        UserManager<User> _userManager;

        /// <summary>
        ///     Method that creates the users controller (add/edit/delete users)
        /// </summary>
        /// <param name="mapper">
        ///     Mapper to create projections of one model in other model
        /// </param>
        /// <param name="userManager">
        ///     User manager to manage them
        /// </param>
        /// <param name="userService">
        ///     Interface for working with user in the database indirectly
        /// </param>
        public UsersController(UserManager<User> userManager, 
            IMapper mapper,
            IUserService userService)
        {
            _mapper = mapper;
            _userManager = userManager;
            _userService = userService;
        }

        /// <summary>
        ///     Method that renders page with all users
        /// </summary>
        /// <returns>
        ///     Page with all users
        /// </returns>
        public IActionResult Index()
        {
            return View(_userManager.Users.ToList());
        }

        /// <summary>
        ///     Method that renders page which has fields for creating a user
        /// </summary>
        /// <returns>
        ///     Page which has fields for creating a user
        /// </returns>
        public IActionResult Create() => View();

        /// <summary>
        ///     Method that process creating user for provided user data
        /// </summary>
        /// <param name="CreateUserModel">
        ///     User data from form
        /// </param>
        /// <returns>
        ///     Main page 
        /// </returns>
        [HttpPost]
        public async Task<IActionResult> Create(CreateUserViewModel CreateUserModel)
        {
            if (ModelState.IsValid)
            {
                User user = new User
                {
                    Email = CreateUserModel.Email,
                    UserName = CreateUserModel.UserName,
                    FirstName = CreateUserModel.FirstName,
                    LastName = CreateUserModel.LastName,
                    Rating = 0
                };
                var result = await _userManager.CreateAsync(user, CreateUserModel.Password);
                if (result.Succeeded)
                {
                    return RedirectToAction("Index", "Home");
                }
                else
                {
                    foreach (var error in result.Errors)
                    {
                        ModelState.AddModelError(string.Empty, error.Description);
                    }
                }
            }
            return View(CreateUserModel);
        }


        /// <summary>
        ///     Method that allows you to edit the user
        /// </summary>
        /// <param name="id">
        ///     Id user with we want eding user
        /// </param>
        /// <returns>
        ///      Page with user eding fields
        /// </returns>
        public async Task<IActionResult> Edit(string id)
        {
            User user = await _userManager.FindByIdAsync(id);
            if (user == null)
            {
                return NotFound();
            }
            EditUserViewModel model = new EditUserViewModel
            {
                Id = user.Id,
                Email = user.Email,
                UserName = user.Email,
                FirstName = user.FirstName,
                LastName = user.LastName
            };
            return View(model);
        }

        /// <summary>
        ///     Method that process edit the user for provided user data
        /// </summary>
        /// <param name="EditUserModel">
        ///     User data from form
        /// </param>
        /// <returns>
        ///     Redirect to page with list all users
        /// </returns>
        [HttpPost]
        public async Task<IActionResult> Edit(EditUserViewModel EditUserModel)
        {
            if (ModelState.IsValid)
            {
                User user = await _userManager.FindByIdAsync(EditUserModel.Id);
                if (user != null)
                {
                    user.Email = EditUserModel.Email;
                    user.FirstName = EditUserModel.FirstName;
                    user.LastName = EditUserModel.LastName;
                    user.UserName = EditUserModel.Email;

                    var result = await _userManager.UpdateAsync(user);
                    if (result.Succeeded)
                    {
                        return RedirectToAction("Index");
                    }
                    else
                    {
                        foreach (var error in result.Errors)
                        {
                            ModelState.AddModelError(string.Empty, error.Description);
                        }
                    }
                }
            }
            return View(EditUserModel);
        }

        /// <summary>
        ///     Method that deleting a users
        /// </summary>
        /// <param name="id">
        ///     Id role with we want deleted
        /// </param>
        /// <returns>
        ///     Page with list of all users without one
        /// </returns>
        [HttpPost]
        public async Task<ActionResult> Delete(string id)
        {
            User user = await _userManager.FindByIdAsync(id);
            if (user != null)
            {
                IdentityResult result = await _userManager.DeleteAsync(user);
            }
            return RedirectToAction("Index");
        }
    }
}
