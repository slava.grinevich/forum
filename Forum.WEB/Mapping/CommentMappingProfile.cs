﻿using System.Diagnostics.CodeAnalysis;
using AutoMapper;
using Forum.BLL.DTO;
using Forum.DAL.Entities;
using Forum.WEB.ViewModel;

namespace Forum.WEB.Mapping
{
    public class CommentMappingProfile : Profile
    {
        public CommentMappingProfile()
        {
            CreateMap<Comment, CommentDTO>();

            CreateMap<CommentDTO, Comment>();

            CreateMap<CommentDTO, CommentViewModel>();
        }
    }
}
