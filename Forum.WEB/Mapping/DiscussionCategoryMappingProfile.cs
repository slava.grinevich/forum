﻿using System.Diagnostics.CodeAnalysis;
using AutoMapper;
using Forum.BLL.DTO;
using Forum.DAL.Entities;
using Forum.WEB.ViewModel;
using System.Collections.Generic;

namespace Forum.WEB.Mapping
{
    public class DiscussionCategoryMappingProfile : Profile
    {
        public DiscussionCategoryMappingProfile()
        {
            CreateMap<DiscussionCategory, DiscussionCategoryDTO>()
                .ForMember(x => x.Topics, opt => opt.Ignore());

            CreateMap<DiscussionCategoryDTO, DiscussionCategory>()
                .ForMember(x => x.Topics, opt => opt.Ignore());


            CreateMap<DiscussionCategoryDTO, DiscussionCategoryViewModel>()
                .ForMember(x=>x.Topics, opt=> opt.Ignore());

            CreateMap<DiscussionCategoryViewModel, DiscussionCategoryDTO>()
                .ForMember(x => x.Topics, opt => opt.Ignore());

        }  
    }
}
