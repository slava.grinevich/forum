﻿using System.Diagnostics.CodeAnalysis;
using AutoMapper;
using Forum.BLL.DTO;
using Forum.DAL.Entities;
using Forum.WEB.ViewModel;

namespace Forum.WEB.Mapping
{
    public class DiscussionMappingProfile : Profile
    {
        public DiscussionMappingProfile()
        {
            CreateMap<Discussion, DiscussionDTO>()
                .ForMember(x => x.Comments, opt => opt.Ignore());

            CreateMap<DiscussionDTO, Discussion>()
                .ForMember(x => x.Comments, opt => opt.Ignore());


            CreateMap<DiscussionDTO, DiscussionViewModel>()
                .ForMember(x=>x.Comments, opt=>opt.Ignore());

            CreateMap<DiscussionViewModel, DiscussionDTO>()
                .ForMember(x => x.Comments, opt => opt.Ignore());
        }
    }
}
