﻿using System.Diagnostics.CodeAnalysis;
using AutoMapper;
using Forum.BLL.DTO;
using Forum.DAL.Entities;
using Forum.WEB.ViewModel;

namespace Forum.WEB.Mapping
{
    public class DiscussionTopicMappingProfile : Profile
    {
        public DiscussionTopicMappingProfile()
        {
            CreateMap<DiscussionTopic, DiscussionTopicDTO>()
                .ForMember(x => x.Discussions, opt => opt.Ignore());

            CreateMap<DiscussionTopicDTO, DiscussionTopic>()
                .ForMember(x => x.Discussions, opt => opt.Ignore());

            CreateMap<DiscussionTopicDTO, DiscussionTopicViewModel>()
                .ForMember(x=>x.Discussions, opt=>opt.Ignore());

            CreateMap<DiscussionTopicViewModel, DiscussionTopicDTO>()
               .ForMember(x => x.Discussions, opt => opt.Ignore());
        }
    }
}
