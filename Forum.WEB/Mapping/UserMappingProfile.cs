﻿using System.Diagnostics.CodeAnalysis;
using AutoMapper;
using Forum.BLL.DTO;
using Forum.DAL.Entities;

namespace Forum.WEB.Mapping
{
    public class UserMappingProfile : Profile
    {
        public UserMappingProfile()
        {
            CreateMap<User, UserDTO>()
                .ForMember(x=>x.Comments, c=>c.Ignore());

            CreateMap<UserDTO, User>()
                .ForMember(x => x.Comments, c => c.Ignore());
        }
    }
}
