﻿using System;
using System.Net;
using System.Threading.Tasks;
using Forum.BLL.Exceptions;
using Microsoft.AspNetCore.Http;

namespace Forum.WEB.Middleware
{
    public class ExceptionMiddleware
    {

        private readonly RequestDelegate _next;

        public ExceptionMiddleware(RequestDelegate next)
        {
            _next = next;
        }

        public async Task InvokeAsync(HttpContext httpContext)
        {
            try
            {
                await _next(httpContext);
            }
            //catch (NotFoundException notFoundException)
            //{
            //    await HandleExceptionAsync(
            //        httpContext,
            //        notFoundException,
            //        HttpStatusCode.NotFound,
            //        notFoundException.Message);
            //}
            catch (ValidationException validationException)
            {
                await HandleExceptionAsync(
                    httpContext,
                    validationException,
                    HttpStatusCode.BadRequest,
                    validationException.Message);
            }
            catch (Exception exception)
            {
                await HandleExceptionAsync(
                    httpContext,
                    exception,
                    HttpStatusCode.InternalServerError,
                    "Something went wrong");
            }
        }

        private async Task HandleExceptionAsync(HttpContext context, Exception exception, HttpStatusCode statusCode,
            string errorMessage)
        {
            context.Response.StatusCode = (int)statusCode;

            await context.Response.WriteAsync(errorMessage);
        }
    }
}
