using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Forum.DAL.EF;
using Forum.DAL.Entities;
using Forum.DAL.Interfaces;
using Forum.DAL.Repositories;
using Forum.BLL.DTO;
using Forum.BLL.Interfaces;
using Forum.BLL.Services;
using Microsoft.EntityFrameworkCore;
using AutoMapper;
using Microsoft.AspNetCore.Identity;
using Forum.WEB.Middleware;

namespace Forum.WEB
{
    public class Startup
    {
        private IConfigurationRoot _confString;


        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }


        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<AppDBContent>(options => options.UseSqlServer(Configuration.GetConnectionString("DefaultConnection")));
            services.AddIdentity<User, IdentityRole>(opts =>
            {
                opts.Password.RequiredLength = 5;
                opts.Password.RequireNonAlphanumeric = false;
                opts.Password.RequireLowercase = true;
                opts.Password.RequireUppercase = true;
                opts.Password.RequireDigit = true;
            }).AddEntityFrameworkStores<AppDBContent>();


            services.AddAutoMapper(typeof(Startup));

            services.AddControllersWithViews();

            services.AddTransient<ICommentRepository, CommentRepository>();
            services.AddTransient<IDiscussionCategoryRepository, DiscussionCategoryRepository>();
            services.AddTransient<IDiscussionRepository, DiscussionRepository>();
            services.AddTransient<IDiscussionTopicRepository, DiscussionTopicRepository>();
            services.AddTransient<IUserRepository, UserRepository>();
            services.AddTransient<IUnitOfWork, ForumUnitOfWork>();


            services.AddTransient<ICommentService, CommentService>();
            services.AddTransient<IDiscussionCategoryService, DiscussionCategoryService>();
            services.AddTransient<IDiscussionService, DiscussionService>();
            services.AddTransient<IDiscussionTopicService, DiscussionTopicService>();
            services.AddTransient<IUserService, UserService>();
        }


        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                app.UseHsts();
            }
            app.UseHttpsRedirection();
            app.UseStaticFiles();

            app.UseMiddleware<ExceptionMiddleware>();

            app.UseRouting();

            app.UseAuthentication();    
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller=Home}/{action=StartPage}/{id?}");
            });
        }
    }
}
