﻿namespace Forum.WEB.ViewModel
{
    public class AddDiscussionTopicViewModel
    {
        public DiscussionTopicViewModel discussionTopicViewModel {  get; set; }

        public string TopicId { get; set; }
        public string UserFullName { get; set; }
    }
}
