﻿using System;
using System.Collections.Generic;

namespace Forum.WEB.ViewModel
{
    public class AllDiscussionViewModel
    {
        public IEnumerable<DiscussionViewModel> allDiscussion {  get; set; }
    }
}
