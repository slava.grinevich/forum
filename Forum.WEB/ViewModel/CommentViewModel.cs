﻿using Forum.BLL.DTO;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Forum.WEB.ViewModel
{
    public class CommentViewModel
    {
        public int Id { get; set; }

        public string UserId { get; set; }
        public string UserFullName { get; set; }
        public UserDTO User { get; set; }

        public int DiscussionId { get; set; }
        public DiscussionViewModel Discussion { get; set; }

        [Required(ErrorMessage = "Введите коментарий")]
        [StringLength(500, MinimumLength = 5)]
        public string ComentMessage { get; set; }

        [DataType(DataType.Date)]
        public DateTime DateComent { get; set; }

        public int CountLikesComment { get; set; }
        public int CountDislikesComment { get; set; }
    }
}
