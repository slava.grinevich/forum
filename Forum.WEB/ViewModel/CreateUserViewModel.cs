﻿using System.ComponentModel.DataAnnotations;

namespace Forum.WEB.ViewModel
{
    public class CreateUserViewModel
    {
        [Required(ErrorMessage = "Введите ваше имя")]
        public string FirstName { get; set; }

        [Required(ErrorMessage = "Введите вашу фамилию")]
        public string LastName { get; set; }

        [Required(ErrorMessage = "Введите email")]
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }

        public string UserName { get; set; }

        [Required(ErrorMessage = "Введите пароль")]
        [DataType(DataType.Password)]
        public string Password { get; set; }
    }
}
