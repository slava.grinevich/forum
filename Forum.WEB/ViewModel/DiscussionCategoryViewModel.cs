﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Forum.WEB.ViewModel
{
    public class DiscussionCategoryViewModel
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "Введите имя категории")]
        public string CategotyName { get; set; }

        public List<DiscussionTopicViewModel> Topics { get; set; }

        public int CountAllDiscussion { get; set; }
    }
}
