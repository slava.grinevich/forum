﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Forum.WEB.ViewModel
{
    public class DiscussionTopicViewModel
    {
        public int Id { get; set; }
        [Required(ErrorMessage = "Введите имя темы обсуждния")]
        public string Topic { get; set; }

        public int IdCategory { get; set; }
        public DiscussionCategoryViewModel DiscussionCategory { get; set; }

        public List<DiscussionViewModel> Discussions { get; set; }
    }
}
