﻿using Forum.BLL.DTO;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Forum.WEB.ViewModel
{
    public class DiscussionViewModel
    {

        public int Id { get; set; }

        public string UserId { get; set; }

        public string UserFullName { get; set; }

        [Required(ErrorMessage = "Введите обсуждение")]
        public string DiscussionMessage { get; set; }

        public DateTime DiscussionDate { get; set; }

        public bool IsDiscussed { get; set; }

        public int TopicId { get; set; }

        public List<CommentViewModel> Comments { get; set; }

    }
}
