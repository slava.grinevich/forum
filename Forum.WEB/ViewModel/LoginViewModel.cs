﻿using System.ComponentModel.DataAnnotations;

namespace Forum.WEB.ViewModel
{
    public class LoginViewModel
    {
        [Required]
        //[DataType(DataType.EmailAddress)]
        [Display(Name = "Email")]
        public string Email { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Пароль")]
        public string Password { get; set; }

        [Display(Name = "Запомнить?")]
        public bool RememberMe { get; set; }

        public string ReturnUrl { get; set; }
    }
}